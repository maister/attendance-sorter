#!/usr/bin/env bash

# Use FILTER to remove out any name you don't want to show up
# For example, if you don't want to show your own name, place write
# your first name and middle initial like so

# FILTER="First M"

# For multiple names, wrap all names with \( \) and separate each with \|

# FILTER="\(First M\|First M\|First M\)"

case "$(uname -s)" in
	Linux*)     OS="Linux"   ;;
	Darwin*)    OS="Mac"     ;;
	*)          OS="Unknown" ;;
esac

check_format() {
	for FILE in *; do
		# Checks that files are CSVs
		if [ "$(file -b --mime-type "$FILE")" = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" ]; then
			libreoffice --convert-to csv "$FILE"
		fi
		# Check that files are utf-8
		FILEENC=$(file --brief --mime-encoding "$FILE")
		if grep -sq "UTF-16" "$FILEENC" ; then ## Change UTF16 to std UTF8 if needed:
			BASENAME="$(basename "$FILE")";
			if [ -s "$(which iconv)" ]; then
				iconv -f utf-16 -t utf-8 "$BASENAME" > _"$BASENAME"_ &&
				mv _"$BASENAME"_ "$BASENAME"
				rm _"$BASENAME"_
			fi
		fi
	done
}

modify_filename() {
	# Remove spaces from filenames
	find . -name "* *.csv" -type f -print0 | \
	while read -rd $'\0' f; do mv -v "$f" "${f// /_}"; done

	# Remove parentheses from filename
	for FILE in *.csv; do
		FILE="$(basename "$FILE")";
		RENAME=${FILE//[\(\)]/};
		mv "$FILE" "$RENAME" ;
	done
}

internal_format() {
	for FILE in *.csv; do
		# Remove \" (double-quotes) from within files:
		# Change to tab-separated file
		if [[ "$OS" = "Mac" ]]; then
			sed -i '' 's/\"//g;s/,,,//;s/,/\t/g' "$FILE"
		elif [[ "$OS" = "Linux" ]]; then
			sed -i 's/\"//g;s/,,,//;s/,/\t/g' "$FILE"
		fi
	done
}

# Sort the names alphabetically
sort_attendance() {
	for FILE in *.csv; do
		if [ -n "$FILTER" ]; then
			grep Joined "$FILE" | grep -v "$FILTER" | sort -t $'\011' -k 3,3 -u >> .tmp.csv
		else
			grep Joined "$FILE" | sort -t $'\011' -k 3,3 -u >> .tmp.csv
		fi
	done
	sort -t $'\011' -k 2,2 .tmp.csv -o master.csv
	rm .tmp.csv
}

# Executing functions
check_format
modify_filename
internal_format
sort_attendance
