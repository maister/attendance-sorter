# Teams Attendance Sorter

This is a simple Bash script that will sort through a directory of Microsoft Teams attendance files, filter through the participants list such that only unique names are retained along with their ID and date of attendance, and merge all files into one central attendance record.

If you need to store ID numbers, please create an immediately-adjacent column either in the leftmost or rightmost position of the spreadsheet. Basically, you want no empty columns between entries. Additionally, enter the ID of a participant in the first row that they appear on.

This script was created for a specific use case with my department's attendance requirements, and published mainly for demonstration purposes. Your use of this script is at your own risk, and you are free to modify its contents locally as you wish.

## Installation

You can obtain the script by git cloning and moving it to your `$PATH` directory.

```
git clone https://gitlab.com/maister/attendance-sorter
```

Alternatively, you can download the repository manually using the blue download button on the top right. Then, move the script into the directory of your attendance files.

## Optional Dependencies

* `libreoffice` for conversion from XLSX to CSV
* `iconv` for conversion from UTF-16 to UTF-8

You may not need these dependencies if you manually convert attendance files to UTF-8 encoded CSVs yourself.


## Usage

1. Place all attendance files into one folder
2. From the terminal `cd /path/to/your/directory`
3. Run either `sorter` if `sorter` in `$PATH` or `./sorter` if moved manually

That's it. If no errors occur, you should see a tab-separated `master.csv` with all unique participants entered. Import `master.csv` in your spreadsheet editor and choose UTF-8 encoding.

#### Before
![](img/before.png)

#### After
![](img/after.png)

## License

This script is licensed under [GNU General Public License](LICENSE.md).
